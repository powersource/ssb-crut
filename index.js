const Strategy = require('@tangle/strategy')
const Overwrite = require('@tangle/overwrite')
const { tombstone } = require('ssb-schema-definitions')()
const { promisify } = require('util')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')

const isValidSpec = require('./lib/is-valid-spec')
const IsValidMsg = require('./lib/is-valid-msg')
const IsValidInput = require('./lib/is-valid-input')
const ResolveTangle = require('./lib/resolve-tangle')
const HandlePublish = require('./lib/handle-publish')
const CreateStream = require('./lib/create-stream')
const { merge, isObject, pick, defaultGetTransformation } = require('./lib/util')

const {
  PROTECTED_PROPS,
  PROTECTED_STATIC_PROPS,
  PROTECTED_NEXT_STEP_DATA
} = require('./constants')

module.exports = class CRUT {
  constructor (ssb, spec, opts = {}) {
    checkServerDeps(ssb)
    checkReservedValues(spec)
    checkOpts(opts)

    const {
      publish = ssb.publish,
      feedId = ssb.id
    } = opts

    this.spec = merge(
      {
        typePattern: spec.typePattern || `^${spec.type}$`,
        tangle: spec.type && spec.type.split('/')[0],
        props: {
          tombstone: Overwrite({ valueSchema: tombstone })
        },
        staticProps: {},
        nextStepData: {},
        isValidNextStep: () => true,
        hooks: {
          isRoot: (spec && spec.hooks && spec.hooks.isRoot) || [],
          isUpdate: (spec && spec.hooks && spec.hooks.isUpdate) || []
        },
        getTransformation: defaultGetTransformation,
        arbitraryRoot: false
      },
      spec
    )
    if (!isValidSpec(this.spec)) throw isValidSpec.error
    this.strategy = new Strategy(this.spec.props)

    this.isRoot = IsValidMsg.root(this)
    this.isUpdate = IsValidMsg.update(this)
    this.isValidUpdateInput = IsValidInput('update input', {
      ...this.spec.props,
      ...this.spec.nextStepData
    })

    this.resolveTangle = ResolveTangle(ssb, this)
    this.handlePublish = HandlePublish({ crut: this, publish, feedId, ssb })

    if (this.spec.arbitraryRoot) this.getTribe = (groupId, cb) => ssb.tribes.get(groupId, cb)
    // NOTE ssb.tribes is not accessible while in ssb-tribes#init, this side-steps that

    this.createStream = CreateStream(ssb, this.spec)
  }

  create (input, cb) {
    if (cb === undefined) return promisify(this.create).call(this, input)

    if (this.spec.arbitraryRoot) return cb(new Error('cannot call create on a crut with spec.arbitraryRoot'))
    if (!isObject(input)) return cb(new Error('input must be an Object'))

    const {
      props, staticProps, nextStepData, recps, allowPublic, unknown
    } = sortInput(this.spec, input)
    if (Object.keys(unknown).length) {
      return cb(new Error(`unallowed inputs: ${Object.keys(unknown)}`))
    }

    let propsT
    try {
      propsT = this.strategy.mapFromInput(
        props,
        [this.strategy.identity()]
      )
    } catch (e) { return cb(e) }

    const content = {
      type: this.spec.type,
      ...nextStepData,
      ...staticProps,
      ...propsT,

      recps,
      tangles: {
        [this.spec.tangle]: { root: null, previous: null }
      }
    }

    // we mock a an empty initial transformation state so we can check `isValidNextStep`
    // on the initial step (root message)
    const tangleContext = {
      root: null, // DNE
      tips: [{
        key: null, // DNE
        T: this.strategy.identity()
      }],
      graph: null // DNE
    }

    this.handlePublish(tangleContext, content, allowPublic, cb)
  }

  read (id, cb) {
    if (cb === undefined) return promisify(this.read).call(this, id)

    this.resolveTangle(id, (err, tangle) => {
      if (err) return cb(err)

      const result = {
        key: id,
        type: this.spec.type,
        originalAuthor: tangle.root.value.author,
        recps: tangle.root.value.content.recps || null,

        ...pick(
          tangle.root.value.content,
          Object.keys(this.spec.staticProps)
        ),
        // ...bestGuessState
        conflictFields: [],

        // legacy
        states: tangle.tips
          .map(({ key, T }) => ({ key, ...this.strategy.mapToOutput(T) }))
      }

      let bestGuessState

      // The transformation props are also spread into result (from states).
      // If there is only one state we can just use that (without it's key):
      if (result.states.length === 1) bestGuessState = { ...result.states[0] }
      // However, if there are multiple states we first try to automatically merge them.
      else {
        const dummyNode = {
          // key: '%dummyMessageId',
          previous: tangle.tips.map(({ key, T }) => key),
          data: this.strategy.identity()
          // author: '%dummyMessageAuthor',
          // sequence: (sequence || 0) + 1
        }
        // If we can automatically merge the tips, then the best guess of state is the merged tips
        if (this.strategy.isValidMerge(tangle.graph, dummyNode)) {
          const T = this.strategy.merge(tangle.graph, dummyNode)
          bestGuessState = this.strategy.mapToOutput(T)
        } // eslint-disable-line
        // Otherwise, we fall back to the tip with the latest entry
        else {
          bestGuessState = { ...result.states[0] }
          result.conflictFields = this.strategy.isValidMerge.fields
        }
      }

      if ('key' in bestGuessState) delete bestGuessState.key
      // Spread the bestGuessState props into result
      Object.assign(result, bestGuessState)

      cb(null, result)
    })
  }

  update (id, input, cb) {
    if (cb === undefined) return promisify(this.update).call(this, id, input)

    const allowPublic = input.allowPublic
    delete input.allowPublic
    if (!this.isValidUpdateInput(input)) return cb(this.isValidUpdateInput.error)

    this.resolveTangle(id, (err, tangle) => {
      if (err) return cb(err)

      const { props, nextStepData } = sortInput(this.spec, input)

      const content = {
        type: this.spec.type,
        ...this.strategy.mapFromInput(
          props,
          tangle.tips.map(tip => tip.T) // currentTips state
        ),
        ...nextStepData,
        recps: tangle.root.value.content.recps,
        tangles: {
          [this.spec.tangle]: {
            root: id,
            previous: tangle.tips.map(tip => tip.key) // currentTips keys
          }
        }
      }

      this.handlePublish(tangle, content, allowPublic, cb)
    })
  }

  list (opts, cb) {
    if (opts === undefined) opts = {}
    if (cb === undefined) return promisify(this.list).call(this, opts)
    if (opts.limit && typeof opts.limit !== 'number') { throw new Error('opts.limit must be a number') }

    let hasReachedStartFrom = false

    pull(
      this.createStream(opts),

      opts.startFrom
        ? pull.filter(msg => {
            if (hasReachedStartFrom) return true
            if (msg.key === opts.startFrom) hasReachedStartFrom = true
            return false
          })
        : null,

      opts.groupId
        ? pull.filter(msg => (
            msg.value.content.recps &&
            msg.value.content.recps[0] === opts.groupId
          ))
        : null,

      // map rootMsg => record (and filter out the ones which were junk)
      paraMap(
        opts.read || ((msg, cb) => {
          this.read(msg.key, (err, record) => {
            if (err) cb(null, false)
            else cb(null, record)
          })
        }),
        opts.width || 5
      ),
      pull.filter(Boolean),

      // filter out the tombstoned
      opts.tombstoned
        ? pull.filter(record => record.tombstone !== null)
        : pull.filter(record => record.tombstone === null),

      opts.filter
        ? pull.filter(opts.filter)
        : null,

      // limit
      opts.limit ? pull.take(opts.limit) : null,
      pull.collect(cb)
    )
  }

  tombstone (id, input, cb) {
    if (cb === undefined) return promisify(this.tombstone).call(this, id, input)
    const _input = Object.keys(input).reduce(
      (acc, key) => {
        if (
          key === 'allowPublic' ||
          key in this.spec.nextStepData
        ) acc[key] = input[key]
        return acc
      },
      {
        tombstone: input.undo === true
          ? null
          : { date: Date.now(), reason: input.reason }
      }
    )

    this.update(id, _input, cb)
  }

  /* special group methods */
  updateGroup (groupId, input, cb) {
    if (cb === undefined) return promisify(this.updateGroup).call(this, groupId, input)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use updateGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.update(info.root, input, cb)
    })
  }

  readGroup (groupId, cb) {
    if (cb === undefined) return promisify(this.readGroup).call(this, groupId)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use readGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.read(info.root, cb)
    })
  }

  tombstoneGroup (groupId, input, cb) {
    if (cb === undefined) return promisify(this.tombstoneGroup).call(this, groupId, input)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use tombstoneGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.tombstone(info.root, input, cb)
    })
  }
}

function sortInput (spec, input) {
  return Object.keys(input).reduce(
    (acc, key) => {
      if (key === 'recps') acc.recps = input[key]
      else if (key === 'allowPublic') acc.allowPublic = input[key]
      else if (key in spec.props) acc.props[key] = input[key]
      else if (key in spec.staticProps) acc.staticProps[key] = input[key]
      else if (key in spec.nextStepData) acc.nextStepData[key] = input[key]
      else acc.unknown[key] = input[key]

      return acc
    },
    {
      props: {},
      staticProps: {},
      nextStepData: {},
      recps: undefined,
      allowPublic: false,
      unknown: {}
    }
  )
}

function checkServerDeps (ssb) {
  const db1Support = Boolean(ssb.backlinks) && Boolean(ssb.query)
  const db2Support = Boolean(ssb.db)

  if (!db1Support && !db2Support) {
    throw new Error('ssb-crut requires EITHER (ssb-db + ssb-backlinks + ssb-query) OR ssb-db2')
  }
}

function checkReservedValues (spec) {
  if (spec.props) {
    Object.keys(spec.props).forEach(prop => {
      if (PROTECTED_PROPS.has(prop)) throw new Error(`Invalid spec: spec.props.${prop} is a reserved property`)
    })
  }
  if (spec.staticProps) {
    Object.keys(spec.staticProps).forEach(prop => {
      if (PROTECTED_STATIC_PROPS.has(prop)) throw new Error(`Invalid spec: spec.staticProps.${prop} is a reserved property`)
    })
  }
  if (spec.nextStepData) {
    Object.keys(spec.nextStepData).forEach(prop => {
      if (PROTECTED_NEXT_STEP_DATA.has(prop)) throw new Error(`Invalid spec: spec.nextStepData.${prop} is a reserved property`)
    })
  }
}

function checkOpts (opts) {
  if (opts.publish) {
    if (typeof opts.publish !== 'function') throw new Error('opts.publish must be a function')
    if (typeof opts.feedId !== 'string') throw new Error('opts.feedId must be provided if opts.publish is used')
  }
}
