const CREATE_TIME = 'createTime'

module.exports = function CreateStream (ssb, spec) {
  if (ssb.db) {
    const { where, and, slowEqual, type, descending, sortByArrival, toPullStream } = require('ssb-db2/operators')

    return function createStreamDB2 (opts) {
      return ssb.db.query(
        where(
          and(
            type(spec.type),
            slowEqual(`value.content.tangles.${spec.tangle}.root`, null)
          )
        ),
        opts.descending === false ? null : descending(),
        (opts.orderBy || opts.sortBy) === CREATE_TIME ? null : sortByArrival(),
        toPullStream()
      )
    }
  } // eslint-disable-line
  else {
    return function createStreamDB1 (opts) {
      const query = [{
        $filter: {
          value: {
            content: {
              type: spec.type,
              tangles: {
                [spec.tangle]: { root: null }
              }
            }
          }
        }
      }]
      if ((opts.orderBy || opts.sortBy) === CREATE_TIME) {
        query[0].$filter.value.timestamp = { $gt: 0 }
        // this is a common ssb-query hack to stimulate the tya index to be used
        // tya = "type, asserted timestamp" (otherwise tyr = "type, received timestamp" is used)
      }

      return ssb.query.read({
        query,
        // ...opts,
        reverse: opts.descending !== false
      })
    }
  }
}
