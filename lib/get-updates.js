const pull = require('pull-stream')

const { getCanonicalContent } = require('./util')

const B_VALUE = Buffer.from('value')
const B_CONTENT = Buffer.from('content')
const B_TANGLES = Buffer.from('tangles')
const B_ROOT = Buffer.from('root')

module.exports = function GetUpdates (ssb, crut) {
  return ssb.db // test is isDB2
    ? GetUpdates2(ssb, crut)
    : GetUpdates1(ssb, crut)
}

function GetUpdates1 (ssb, crut) {
  const { spec: { type, tangle, getTransformation } } = crut
  const isUpdate = msg => crut.isUpdate(getCanonicalContent(msg, getTransformation))

  return function getUpdates (id, cb) {
    const query = [{
      $filter: {
        dest: id,
        value: {
          content: {
            type,
            tangles: {
              [tangle]: { root: id }
            }
          }
        }
      }
    }]

    pull(
      ssb.backlinks.read({ query }),
      pull.filter(isUpdate),
      pull.collect(cb)
    )
  }
}

function GetUpdates2 (ssb, crut) {
  const { seekKey } = require('bipf')
  const { spec } = crut
  const isUpdate = msg => crut.isUpdate(getCanonicalContent(msg, spec.getTransformation))
  const B_TANGLE = Buffer.from(spec.tangle)

  function tangleRoot (value) {
    return ssb.db.operators.equal(seekTangleRoot, value, {
      indexType: `value.content.tangles.${spec.tangle}.root`
    })
  }
  function seekTangleRoot (buffer) {
    let p = 0 // note you pass in p!
    p = seekKey(buffer, p, B_VALUE)
    if (p < 0) return
    p = seekKey(buffer, p, B_CONTENT)
    if (p < 0) return
    p = seekKey(buffer, p, B_TANGLES)
    if (p < 0) return
    p = seekKey(buffer, p, B_TANGLE)
    if (p < 0) return
    return seekKey(buffer, p, B_ROOT)
  }

  return function getUpdates (id, cb) {
    const { where, and, type, toPullStream } = ssb.db.operators
    pull(
      ssb.db.query(
        where(
          and(
            type(spec.type),
            tangleRoot(id)
          )
        ),
        toPullStream()
      ),
      pull.filter(isUpdate),
      pull.collect(cb)
    )
  }
}
