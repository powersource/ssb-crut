const Strategy = require('@tangle/strategy')

module.exports = function isValidSpec (input) {
  const {
    type,
    props,

    tangle,
    isValidNextStep,
    staticProps,
    nextStepData,
    hooks,
    getTransformation,
    arbitraryRoot
  } = input

  isValidSpec.error = null
  const errors = []

  /* Required */

  // type
  if (typeof type !== 'string') errors.push('spec.type must be a String')

  // props
  Object.entries(props).forEach(([field, strategy]) => {
    if (!Strategy.isValid(strategy)) errors.push(`spec.props.${field} must be a valid strategy ${Strategy.isValid.error}`)
  })

  /* Optional (constructor has provide) */

  // tangle
  if (typeof tangle !== 'string' || tangle.length === 0) errors.push('spec.tangle must be a String')
  if (tangle === 'group') errors.push('spec.tangle cannot be "group" - reserved tangle')

  // isValidNextStep
  if (typeof isValidNextStep !== 'function') errors.push('spec.isValidNextStep must be a function')

  // staticProps
  if (typeof staticProps !== 'object') errors.push('spec.staticProps must be an Object')
  else {
    if (!isInputSpecShaped(staticProps)) { errors.push('spec.staticProps were malformed') }

    // check staticProps don't collide with props
    const duplicates = findDuplicateKeys(staticProps, props)
    if (duplicates.length) errors.push(`staticProps with keys same as props: [${duplicates}]`)
  }

  // nextStepData
  if (typeof nextStepData !== 'object') errors.push('spec.nextStepData must be an Object')
  else {
    if (!isInputSpecShaped(nextStepData)) { errors.push('spec.nextStepData were malformed') }

    // check nextStepData don't collide with props
    const duplicates1 = findDuplicateKeys(nextStepData, props)
    if (duplicates1.length) errors.push(`nextStepData with keys same as props: [${duplicates1}]`)

    // check nextStepData don't collide with staticProps
    const duplicates2 = findDuplicateKeys(nextStepData, staticProps)
    if (duplicates2.length) errors.push(`nextStepData with keys same as staticProps: [${duplicates2}]`)
  }

  // hooks
  if (typeof hooks !== 'object') errors.push('spec.hooks must be an Object')
  else {
    if (!Array.isArray(hooks.isRoot)) errors.push('spec.hooks.isRoot must be an Array')
    else {
      // check every entry is a function
      const isProblem = hooks.isRoot.some((fn) => {
        return typeof fn !== 'function'
      })
      if (isProblem) errors.push('spec.hooks.isRoot only takes functions')
    }

    if (!Array.isArray(hooks.isUpdate)) errors.push('spec.hooks.isUpdate must be an Array')
    else {
      // check every entry is a function
      const isProblem = hooks.isUpdate.some((fn) => {
        return typeof fn !== 'function'
      })
      if (isProblem) errors.push('spec.hooks.isUpdate only takes functions')
    }
  }

  // getTransformation
  if (typeof getTransformation !== 'function') errors.push('spec.getTransformation must be Function')

  // arbitraryRoot
  if (typeof arbitraryRoot !== 'boolean') errors.push('spec.arbitraryRoot must be Boolean')

  /* Results */
  if (errors.length === 0) return true

  isValidSpec.error = new Error(['Invalid spec:', ...errors].join('\n- '))
  return false
}

function isInputSpecShaped (props) {
  return Object.entries(props).every(([prop, description]) => {
    if (typeof prop !== 'string') return false
    else if (typeof description !== 'object') return false
    else return true
  })
}

function findDuplicateKeys (toTest, alreadyInUse) {
  return Object.keys(toTest).filter(key => key in alreadyInUse)
}
