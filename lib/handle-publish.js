const stringify = require('fast-json-stable-stringify')
const { createHash } = require('crypto')

const { defaultGetTransformation, getCanonicalContent, MsgToNode } = require('./util')

module.exports = function HandlePublish ({ crut, publish, feedId, ssb }) {
  const { spec, isRoot, isUpdate } = crut
  const msgToNode = MsgToNode(spec)

  return function handlePublish (tangleContext, content, allowPublic, cb) {
    const previous = content.tangles[spec.tangle].previous
    const isRootContent = previous === null

    if (isRootContent) {
      if (!isRoot(content)) return cb(new Error(isRoot.errorsString))
    } else {
      if (!isUpdate(content)) return cb(new Error(isUpdate.errorsString))
    }

    buildDummyMsg(content, (err, dummyMsg) => {
      err = err || findGetTransformationError(content, dummyMsg, isRootContent)
      // ensure results of getTransformation still conform to schema
      // so we can guarentee reduce will work
      if (err) return cb(err)

      const dummyNode = msgToNode(dummyMsg)
      // if this is a merge update, check if it would be a valid merge.
      if (previous && previous.length > 1) {
        if (!crut.strategy.isValidMerge(tangleContext.graph, dummyNode)) {
          const error = crut.strategy.isValidMerge.error
          if (!error) return cb(invalidMergeError())

          error.conflictFields = crut.strategy.isValidMerge.fields
          return cb(error)
        }
      }
      // check is whether it's a valid next step (according to custom function)
      if (!spec.isValidNextStep(tangleContext, dummyNode, ssb)) {
        return cb(spec.isValidNextStep.error || invalidNextStepError(isRootContent))
      }

      publish(guard(content, allowPublic), (err, msg) => {
        if (err) return cb(err)

        cb(null, msg.key)
      })
    })
  }

  function buildDummyMsg (content, cb) {
    ssb.getFeedState(feedId, (err, { sequence } = {}) => {
      if (err) return cb(err)

      cb(null, {
        key: '%dummyMessageId', // trick ssb-msg-content
        value: {
          sequence: (sequence || 0) + 1,
          timestamp: Date.now(),
          author: feedId,
          content,
          // NOTE we trust the programmer not to mutate the content with getTransformation
          meta: {
            dummyMsg: true
          }
        }
      })
    })
  }

  function findGetTransformationError (content, dummyMsg, isRootContent) {
    if (spec.getTransformation === defaultGetTransformation) return

    const checkSum = hash(content)

    // we map msg into canonical form of content that we can use to validate with isRoot/ isUpdate
    const canonicalContent = getCanonicalContent(dummyMsg, spec.getTransformation)

    if (isRootContent) {
      if (!isRoot(canonicalContent)) return new Error(isRoot.errorsString)
    } else {
      if (!isUpdate(canonicalContent)) return new Error(isUpdate.errorsString)
    }

    // check to see if getTransformation has mutated the content we're about to publish
    if (hash(content) !== checkSum) return new Error('getTransformation mutated content about to be published')
  }
}

function hash (input) {
  const h = createHash('sha256')
  h.update(stringify(input))

  return h.digest('hex')
}

function guard (content, allowPublic) {
  // this is for ssb-recps-guard
  return allowPublic
    ? { content, options: { allowPublic: true } }
    : content
}

function invalidNextStepError (isRoot = true) {
  return new Error(isRoot
    ? 'Invalid root message, failed isValidNextStep, publish aborted'
    : 'Invalid update message, failed isValidNextStep, publish aborted'
  )
}

function invalidMergeError () {
  return new Error('Invalid message, failed merge, publish aborted')
}
