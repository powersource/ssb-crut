const merge = require('lodash.merge')

function isObject (obj) {
  if (obj == null) return false
  return typeof obj === 'object'
}

function pick (source, allowed, fallback = null) {
  return allowed.reduce((acc, field) => {
    acc[field] = (field in source) ? source[field] : fallback

    return acc
  }, {})
}

function defaultGetTransformation (m, distance) {
  if (!m.value) console.error(m)
  return m.value.content
}

// CRUT expects to handle messages with content of format:
// {
//   type,
//   ...staticProps, (for root msg)
//   ...nextStepData,
//   ...props,       (includes auto-included field: tombstone)
//   tangles,
//   recps
// }
//
// So this uses getTransformation to generate canonical shaped content for validations
// i.e. isRoot, isUpdate

function getCanonicalContent (msg, getTransformation) {
  if (getTransformation === defaultGetTransformation) return getTransformation(msg)
  // save making more objects unless we need to!

  const content = {
    type: msg.value.content.type,

    ...getTransformation(msg, 0), // distance = 0 is made up...

    tangles: msg.value.content.tangles
    // recps ?
  }
  if (msg.value.content.recps) content.recps = msg.value.content.recps

  return content
}

function MsgToNode (spec) {
  return function msgToNode (msg) {
    return {
      key: msg.key,
      previous: msg.value.content.tangles[spec.tangle].previous,
      data: spec.getTransformation(msg),
      // Note: getTransformation may include props that are not in the strategy

      author: msg.value.author,
      sequence: msg.value.sequence
    }
  }
}

module.exports = {
  isObject,
  pick,
  merge,
  defaultGetTransformation,
  getCanonicalContent,
  MsgToNode
}
