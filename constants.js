
const createInputs = [
  // ...props
  // ...staticProps
  // ...nextStepData
  'recps',
  'allowPublic'
]

const updateInputs = [
  // ...props
  // ...nextStepData
  'allowPublic'
]

const tombstoneInputs = [
  'undo',
  'reason',
  // ...nextStepData
  'allowPublic'
]

const contentFields = [
  'type',
  // ...props
  'tombstone', // auto-provided prop
  // ...staticProps
  // ...nextStepData
  'tangles',
  'recps'
]

const readOutput = [
  'key',
  'type',
  'originalAuthor',
  'recps',
  // ...staticProps
  // ...props
  'tombstone', // auto-provided prop
  'conflictFields',

  'states' // legacy
]

const nono = new Set([
  ...createInputs,
  ...updateInputs,
  ...tombstoneInputs,
  ...contentFields,
  ...readOutput
])

console.log(Array.from(nono).sort().join(', '))

module.exports = {
  PROTECTED_PROPS: nono,
  PROTECTED_STATIC_PROPS: nono,
  PROTECTED_NEXT_STEP_DATA: nono
}
