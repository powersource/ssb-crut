const test = require('tape')
const Overwrite = require('@tangle/overwrite')
const { promisify } = require('util')

const { SSB, replicate } = require('./helpers')
const CRUT = require('../')

test('list', async t => {
  const spec = {
    type: 'profile/test',
    tangle: 'profile',
    props: {
      name: Overwrite()
    }
  }

  // make a friend who has made a record first
  const friend = SSB()
  await new CRUT(friend, spec).create({ name: 0 })

  // meanwhile I publish a bunch of records
  const me = SSB({ tribes: !process.env.DB2 })
  const crut = new CRUT(me, spec)
  const create = (name, recps) => crut.create({ name, recps })

  await create(1)
  const recordId = await create(2)
  await crut.update(recordId, { name: 2.0 })
  await create(3)

  let list

  list = await crut.list()
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0, 1],
    'no opts'
  )

  // limit
  list = await crut.list({ limit: 2 })
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0],
    'opts.limit'
  )

  // limit fail
  try {
    list = await crut.list({ limit: '2' })
  } catch (ex) {
    t.equal(ex.message, 'opts.limit must be a number', 'opts.limit string')
  }

  // ascending
  list = await crut.list({ descending: false })
  t.deepEqual(
    list.map(record => record.name),
    [1, 2.0, 3],
    'opts.descending = false (ascending)'
  )

  // startFrom
  list = await crut.list({ startFrom: recordId })
  t.deepEqual(
    list.map(record => record.name),
    [1],
    'opts.startFrom'
  )

  // orderBy
  await replicate({ from: friend, to: me })

  list = await crut.list()
  t.deepEqual(
    list.map(record => record.name),
    [0, 3, 2.0, 1],
    'opts.orderBy = receivedTime (after replicate))'
  )

  list = await crut.list({ orderBy: 'createTime' })
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0, 1, 0],
    'opts.orderBy = createdTime (after replicate))'
  )

  /* opts.filter */
  list = await crut.list({
    filter: record => Math.floor(record.name % 2 === 0)
  })
  t.deepEqual(
    list.map(record => record.name),
    [0, 2.0], // NOTE 'receivedTime' is the default orderBy
    'opts.filter'
  )

  /* opts.read */
  list = await crut.list({
    read: (msg, cb) => {
      crut.read(msg.key, (err, record) => {
        if (err) return cb(null, null)

        record.fancyName = 'fancy-' + record.name
        delete record.states
        cb(null, record)
      })
    }
    // we provide a custom read which adjusts
    // but could also check a cache etc
  })
  t.deepEqual(
    list.map(record => record.fancyName),
    ['fancy-0', 'fancy-3', 'fancy-2', 'fancy-1'],
    'opts.read'
  )

  // tombstone
  await crut.tombstone(recordId, {})
  list = await crut.list()
  t.deepEqual(
    list.map(record => record.name),
    [0, 3, 1], // NOTE 'receivedTime' is the default orderBy
    'tombstone ignored'
  )
  list = await crut.list({ tombstoned: true })
  t.deepEqual(
    list.map(record => record.name),
    [2.0],
    'opts.tombstoned = true'
  )

  if (!process.env.DB2) {
    /* opts.groupId */
    const { groupId } = await promisify(me.tribes.create)({})
    await create(4, [groupId])
    list = await crut.list({ groupId })
    t.deepEqual(
      list.map(record => record.name),
      [4],
      'opts.groupId'
    )
  }

  me.close()
  friend.close()
  t.end()
})
