const test = require('tape')
const { promisify: p } = require('util')
const Overwrite = require('@tangle/overwrite')

const { SSB, fixRecps } = require('../helpers')
const CRUT = require('../..')

test('opts.arbitraryRoot', async t => {
  const ssb = SSB({ tribes: true })
  const spec = {
    type: 'settings',
    props: {
      autoFollow: Overwrite({ valueSchema: { type: 'boolean' } })
    },
    arbitraryRoot: true
  }
  const settings = new CRUT(ssb, spec)

  /* calling #create */
  const err = await settings.create({ autoFollow: true }).catch(err => err)
  t.match(err.message, /cannot call create on a crut with spec.arbitraryRoot/, 'cannot call #create')

  /* public root */
  let root = await p(ssb.publish)({ type: 'profile' })
  let updateId = await settings.update(root.key, { autoFollow: true })
  let update = await p(ssb.get)({ id: updateId, private: true })
  t.deepEqual(
    update.content,
    fixRecps({
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: {
          root: root.key, previous: [root.key]
        }
      }
    }, ssb),
    'public #update'
  )
  let result = await settings.read(root.key)
  t.deepEqual(
    result,
    {
      key: root.key,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: null,
      states: [{
        key: updateId,
        autoFollow: true,
        tombstone: null
      }],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'public #read'
  )
  // TODO write test with multiple updates?

  /* private root */
  root = await p(ssb.publish)({ type: 'profile', recps: [ssb.id] })
  updateId = await settings.update(root.key, { autoFollow: true })
  update = await p(ssb.get)({ id: updateId, private: true })

  t.deepEqual(
    update.content,
    {
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: {
          root: root.key, previous: [root.key]
        }
      },
      recps: [ssb.id]
    },
    'private #update'
  )

  result = await settings.read(root.key)
  t.deepEqual(
    result,
    {
      key: root.key,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: [ssb.id],
      states: [{
        key: updateId,
        autoFollow: true,
        tombstone: null
      }],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'private #read'
  )

  // db2 doesn't support tribes (yet)
  if (ssb.db) {
    ssb.close()
    t.end()
    return
  }

  /* private group root */
  const { groupId, groupInitMsg } = await p(ssb.tribes.create)({})
  root = groupInitMsg.key
  updateId = await settings.updateGroup(groupId, { autoFollow: true })
  update = await p(ssb.get)({ id: updateId, private: true })

  // HACK - we were accidentally mutating our feedId into the recps (instead of sneaking into key_slots)
  // TODO delete this line when ssb-tribes is updated
  update.content.recps = update.content.recps.filter(r => r !== ssb.id)

  t.deepEqual(
    update.content,
    {
      type: 'settings',
      autoFollow: { set: true },
      tangles: {
        settings: { root, previous: [root] },
        group: { root, previous: [root] }
      },
      recps: [groupId]
    },
    'private-group #updateGroup'
  )

  result = await settings.readGroup(groupId)
  t.deepEqual(
    result,
    {
      key: root,
      originalAuthor: ssb.id,
      type: 'settings',
      recps: [groupId],
      states: [{
        key: updateId,
        autoFollow: true,
        tombstone: null
      }],
      autoFollow: true,
      tombstone: null,
      conflictFields: []
    },
    'private #readGroup'
  )

  await settings.tombstoneGroup(groupId, {})

  ssb.close()
  t.end()
})

test('opts.arbitraryRoot (false, then use *Group methods)', async t => {
  const ssb = SSB({ tribes: true })

  // db2 doesn't support tribes (yet)
  if (ssb.db) {
    ssb.close()
    t.end()
    return
  }

  const spec = {
    type: 'settings',
    props: {
      autoFollow: Overwrite({ valueSchema: { type: 'boolean' } })
    },
    arbitraryRoot: false // << NOTE difference!
  }
  const settings = new CRUT(ssb, spec)

  const { groupId } = await p(ssb.tribes.create)({})

  const err = await settings.updateGroup(groupId, { autoFollow: true })
    .catch(err => err)

  t.match(err.message, /cannot use updateGroup with spec.arbitraryRoot = false/, 'useful error')

  ssb.close()
  t.end()
})
