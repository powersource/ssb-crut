const test = require('tape')
const Overwrite = require('@tangle/overwrite')

const { SSB } = require('../helpers')
const CRUT = require('../..')

test('opts.isValidNextStep (with nextStepData)', async t => {
  t.plan(6)
  const ssb = SSB()

  const spec = {
    type: 'profile/person',
    props: {
      preferredName: Overwrite()
    },
    nextStepData: {
      info: { type: 'integer' }
    },

    isValidNextStep (tangleContext, node) {
      t.true(node.data.info, `got nextStepData: { info: ${node.data.info} }`)
      // NOTE - the whole graph gets re-validated for each update

      return true
    }
  }

  const crut = new CRUT(ssb, spec)

  console.log('create:')
  const id = await crut.create({ preferredName: 'mi', info: 1 })
  console.log('update:')
  await crut.update(id, { preferredName: 'miii', info: 2 })
  console.log('tomstone:')
  await crut.tombstone(id, { reason: 'meh', info: 3 })

  ssb.close()
  t.end()
})
