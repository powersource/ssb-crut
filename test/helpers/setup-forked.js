const pull = require('pull-stream')
const { promisify } = require('util')

const SSB = require('./ssb')
const Spec = require('./spec.mock.js')
const CRUT = require('../..')

// Given some mockSpec prop values for the initial state and fork states,
// setupForked sets up a ssb with these msgs published and returns the ssb and keys.

/*
        initial
        /     \
  forks[0]   forks[1]

*/

module.exports = function setupForked (ssb, initial, forks, cb) {
  if (cb === undefined) return promisify(setupForked)(ssb, initial, forks)

  const spec = Spec({ isValidNextStep: () => true })
  const crut = new CRUT(ssb, spec)

  // Example of `initial`:
  // {
  //   parent: 'Taranga',
  //   preferredName: 'Māui',
  //   attendees: {
  //     add: [{ id: Māui, seq: 33 }]
  //   }
  // }

  let count = 0

  crut.create(initial, (err, rootId) => {
    if (err) throw err

    const currentTip = crut.strategy.mapFromInput(
      crut.strategy.mapToPure(initial),
      [crut.strategy.identity()]
    )

    pull(
      pull.values(forks),
      pull.map(fork => crut.strategy.mapFromInput(fork, [currentTip])),
      pull.asyncMap((fork, cb) => {
        // fork example: { preferredName: { set: 'Māui A' } }
        const manualUpdate = {
          type: spec.type,
          ...fork,
          tangles: {
            [spec.tangle]: { root: rootId, previous: [rootId] }
          }
        }

        if (!crut.isUpdate(manualUpdate)) return cb(new Error(crut.isUpdate.errorsString))

        if (count === 0) ssb.publish(manualUpdate, cb)
        else otherPublish(manualUpdate, cb)
        count++
      }),
      pull.map(msg => msg.key),
      pull.collect((err, forkKeys) => {
        if (err) throw err

        cb(null, {
          spec,
          crut,
          rootId,
          forkKeys
        })
      })
    )
  })

  function otherPublish (manualUpdate, cb) {
    // NOTE this ensures all fork message are created by another peer
    const other = SSB()
    other.publish(manualUpdate, (err, msg) => {
      other.close()
      if (err) return cb(err)
      ssb.add(msg.value, (err) => err ? cb(err) : cb(null, msg))
      // replicates the fork data to the main peer
    })
  }
}
