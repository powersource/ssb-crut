const { replicate } = require('scuttle-testbot')

module.exports = {
  SSB: require('./ssb'),
  Spec: require('./spec.mock.js'),
  replicate,
  fixRecps,
  expectedState,
  setupForked: require('./setup-forked')
}

function fixRecps (obj, ssb) {
  // this is a db2 compat thing
  return Object.assign(
    obj,
    ssb.db ? { recps: undefined } : undefined
  ) // db2 leaves these
}

// Returns the best guess state that is the first (or only) of the tangle tips without the key.
function expectedState (states) {
  const state = { ...states[0] }
  delete state.key
  // WARNING - this is not exactly how expectedState works every time!
  // in some cases if the tips are auto-mergeable (no conflicts), then you get a merged state

  return state
}
