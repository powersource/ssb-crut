const test = require('tape')

const { Spec } = require('./helpers')
const CRUT = require('../')
const { PROTECTED_PROPS, PROTECTED_STATIC_PROPS } = require('../constants')

// mock ssb!
const ssb = process.env.DB2
  ? { db: true }
  : { backlinks: true, query: true }

let spec

test('new Crut', t => {
  /* ssb checks */
  const dudSSB = process.env.DB2
    ? { db: undefined }
    : { backlinks: true, query: undefined }
  t.throws(
    () => new CRUT(dudSSB, Spec()),
    /ssb-crut requires EITHER.*ssb.db \+ ssb-backlinks \+ ssb-query.*OR ssb-db2/,
    'ssb with missing deps throws'
  )

  /* spec checks */
  t.throws(
    () => new CRUT(ssb, {}),
    /Invalid spec/,
    'spec missing type throws'
  )

  /* protected tangle */
  t.throws(
    () => new CRUT(ssb, Spec({ tangle: 'group' })),
    /Invalid spec/,
    'spec.tangle = group throws'
  )
  t.throws(
    () => new CRUT(ssb, Spec({ type: 'group/something', tangle: undefined })),
    /Invalid spec/,
    'spec.type = "group/something" throws (when tangle not set)'
  )

  /* protected props */
  PROTECTED_PROPS.forEach(prop => {
    const spec = Spec()
    spec.props[prop] = 'boop'
    t.throws(
      () => new CRUT(ssb, spec),
      new RegExp(`Invalid spec: spec.props.${prop} .*reserved`),
      `spec with props.${prop} throws`
    )
  })

  /* protected props */
  PROTECTED_STATIC_PROPS.forEach(prop => {
    const spec = Spec()
    spec.staticProps[prop] = 'boop'
    t.throws(
      () => new CRUT(ssb, spec),
      /Invalid spec: spec.staticProps.* reserved/,
      `spec with staticProps.${prop} throws`
    )
  })

  /* protected props */
  PROTECTED_STATIC_PROPS.forEach(prop => {
    const spec = Spec()
    spec.nextStepData[prop] = 'boop'
    t.throws(
      () => new CRUT(ssb, spec),
      /Invalid spec: spec.nextStepData.* reserved/,
      `spec with nextStepData.${prop} throws`
    )
  })

  spec = Spec()
  const propName = Object.keys(spec.props)[0]
  spec.staticProps[propName] = { type: 'string' }
  t.throws(
    () => new CRUT(ssb, spec),
    new RegExp(`staticProps with keys same as props: \\[${propName}\\]`),
    `spec which has ${propName} in both props + staticProps throws`
  )

  t.throws(
    () => new CRUT({}, Spec()), // mock ssb missing backlinks
    /requires .* ssb-backlinks/,
    'missing ssb-backlinks throws'
  )

  t.end()
})
