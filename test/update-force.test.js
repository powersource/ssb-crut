const test = require('tape')
const { promisify } = require('util')

const { SSB, expectedState, setupForked } = require('./helpers')

function ForceUpdate (crut) {
  return function forceUpdate (id, details, cb) {
    crut.update(id, details, (err, updateId) => {
      if (!err) return cb(null, updateId)
      if (!err.conflictFields) return cb(err)

      crut.read(id, (err, record) => {
        if (err) return cb(err)

        for (const field of record.conflictFields) {
          details[field] = record.states[0][field]
          // we grab the first state because states are ordered by update time
        }
        // NOTE this will only work with the @tangle/overwrite fields
        // but so far these are the only ones which conflict!

        crut.update(id, details, cb)
      })
    })
  }
}

/*
Context: this is just a demo of how you could force an update to always go through if you wanted...

NOTE
- this particular "forceUpdate" resolves conflicts for @tangle/overwrite
by elevating the branch with the most recent edit on that field to the winner
- see WARNING below about shortcomings
*/

test('update-force (force update a forked state)', { objectPrintDepth: 6 }, async t => {
  console.log('This is a DEMO')
  const ssb = SSB()
  const Māui = ssb.id

  const { spec, crut, rootId } = await setupForked(
    ssb,
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    [
      // older update
      {
        preferredName: 'Māui A',
        legalName: 'Māui Au'
      },
      // newer update
      {
        preferredName: 'Māui B'
      }

      // NOTE - these branches conflict on `preferredName`, but not on `legalName`
      // our forceUpdate only resolves the preferredName
    ]
  )
    .catch(err => console.log(err))

  const { conflictFields } = await crut.update(rootId, {}).catch(err => err)
  t.deepEqual(conflictFields, ['preferredName'], 'crut.update which doesnt resolve conflict errors')

  /* now see what crut.update does */
  crut.forceUpdate = promisify(ForceUpdate(crut))

  const updateId = await crut.forceUpdate(rootId, {})
  const profile = await crut.read(rootId)

  const states = [
    {
      key: updateId,
      preferredName: 'Māui B', // ends up with newest state for preferredName
      legalName: 'Māui Au',
      attendees: {
        [Māui]: [{ start: 33, end: null }]
      },
      tombstone: null
    }
  ]

  const expected = {
    key: rootId,
    type: spec.type,
    originalAuthor: Māui,
    parent: 'Taranga',
    child: null,
    recps: null,
    states,
    ...expectedState(states),
    conflictFields: []
  }
  t.deepEqual(
    profile,
    expected,
    'merges the current states (overwriting the preferredName)'
  )

  ssb.close()
  t.end()
})
