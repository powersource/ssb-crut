const test = require('tape')
const Overwrite = require('@tangle/overwrite')

const Crut = require('../..')
const Force = require('../../lib/force')
const { SSB, replicate } = require('../helpers')

function buildCrut (ssb) {
  const crut = new Crut(ssb, {
    type: 'profile',
    props: {
      preferredName: Overwrite()
    }
  })
  const { forceUpdate, forceTombstone } = Force(crut)
  crut.forceUpdate = forceUpdate
  crut.forceTombstone = forceTombstone

  return crut
}

test('forceUpdate', async t => {
  const server1 = SSB()
  const server2 = SSB()

  const crut1 = buildCrut(server1)
  const crut2 = buildCrut(server2)

  const profileId = await crut1.create({
    preferredName: 'Maui'
  })

  await replicate({ from: server1, to: server2 })

  await crut1.update(profileId, { preferredName: 'Māui A' })
  await crut2.update(profileId, { preferredName: 'Māui B' })
  await replicate({ from: server2, to: server1 })

  // We do an 'empty' update that does not resolve the conflict we've created.
  await crut1.forceUpdate(profileId, {})
    .catch(err => t.error(err)) // should not run this

  // Then forced update will get the most up to date preferredName
  const profile = await crut1.read(profileId)
  t.deepEqual(
    profile.preferredName,
    'Māui B',
    'returns the Māui B profile (forced merge)'
  )

  t.deepEqual(profile.states.length, 1, 'forcedUpdate merges the branched states')

  server1.close()
  server2.close()
  t.end()
})

test('forceTombstone', async t => {
  const server1 = SSB()
  const server2 = SSB()

  const crut1 = buildCrut(server1)
  const crut2 = buildCrut(server2)

  const profileId = await crut1.create({
    preferredName: 'Maui'
  })

  await replicate({ from: server1, to: server2 })

  await crut1.update(profileId, { preferredName: 'Māui A' })
  await crut2.update(profileId, { preferredName: 'Māui B' })
  await replicate({ from: server2, to: server1 })

  // We tombstone the profile while it has a conflict
  await crut1.forceTombstone(profileId, { reason: 'Mistake' })
    .catch(err => t.error(err)) // should not run this

  // Then forced update will get the most up to date preferredName
  const profile = await crut1.read(profileId)

  t.true(profile.tombstone, 'isTombstoned')
  t.deepEqual(profile.states.length, 1, 'forceTombstone merges the branched states')

  server1.close()
  server2.close()
  t.end()
})
