const test = require('tape')

const { SSB, Spec, expectedState } = require('./helpers')
const CRUT = require('../')

test('tombstone', t => {
  const ssb = SSB()
  const spec = Spec({
    isValidNextStep: ctx => true
  })
  const crut = new CRUT(ssb, spec)

  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui'
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      crut.tombstone(
        profileId,
        {
          reason: 'woops'
        },
        (err, updateId) => {
          t.error(err, 'publish an tombstone')

          ssb.get(updateId, (err, update) => {
            if (err) throw err

            crut.read(profileId, (err, profile) => {
              if (err) throw err

              const states = [{
                key: updateId,
                preferredName: 'Māui',
                legalName: null,
                attendees: {},
                tombstone: {
                  // author: ssb.id,
                  date: update.content.tombstone.set.date,
                  reason: 'woops'
                }
              }]
              const expected = {
                key: profileId,
                type: spec.type,
                originalAuthor: ssb.id,
                parent: 'Taranga',
                child: null,
                recps: null,
                states,
                ...expectedState(states),
                conflictFields: []
              }

              t.deepEqual(profile, expected, 'read tombstoned state')

              crut.tombstone(profileId, { undo: true }, (err, updateId) => {
                t.error(err, 'un-tombstone a record')

                crut.read(profileId, (err, profile) => {
                  if (err) throw err
                  const states = [{
                    key: updateId,
                    preferredName: 'Māui',
                    legalName: null,
                    attendees: {},
                    tombstone: null
                  }]

                  const expected = {
                    key: profileId,
                    type: spec.type,
                    originalAuthor: ssb.id,
                    parent: 'Taranga',
                    child: null,
                    recps: null,
                    states,
                    ...expectedState(states),
                    conflictFields: []
                  }
                  t.deepEqual(profile, expected, 'read tombstoned state')

                  // TODO update after a tombstone! ?
                  ssb.close()
                  t.end()
                })
              })
            })
          })
        }
      )
    }
  )
})

test('tombstone - promise', async t => {
  const ssb = SSB()
  const spec = Spec({
    isValidNextStep: ctx => true
  })
  const crut = new CRUT(ssb, spec)

  const profileId = await crut.create({ parent: 'Taranga', preferredName: 'Māui' })

  const updateId = await crut.tombstone(profileId, { reason: 'woops' })
  const profile = await crut.read(profileId)

  ssb.get(updateId, (err, update) => {
    if (err) throw err

    const states = [{
      key: updateId,
      preferredName: 'Māui',
      legalName: null,
      attendees: {},
      tombstone: {
        // author: ssb.id,
        date: update.content.tombstone.set.date,
        reason: 'woops'
      }
    }]
    const expected = {
      key: profileId,
      type: spec.type,
      originalAuthor: ssb.id,
      parent: 'Taranga',
      child: null,
      recps: null,
      states,
      ...expectedState(states),
      conflictFields: []
    }

    t.deepEqual(profile, expected, 'read tombstoned state')
    t.end()
    ssb.close()
  })
})
